import os
import pygame
import sys
import random

# Cargar el puntaje más alto
if os.path.exists("score.txt"):
    with open("score.txt", "r") as file:
        high_score = int(file.read())
else:
    high_score = 0

# Inicializar Pygame
pygame.init()

canela_image = pygame.image.load("images/canela.png")

# Configuración de la pantalla
width, height = 854, 480
screen = pygame.display.set_mode((width, height))
pygame.display.set_caption("Canela Game")
pygame.display.set_icon(canela_image)

# Inicializar el marcador
score = 0
# Inicializar pausa
pause = False

# Cargar imágenes y escalarlas
player_image = pygame.transform.scale(canela_image, (60, 60))

bullet_image = pygame.image.load("images/corazón.png")
bullet_image = pygame.transform.scale(bullet_image, (50, 50))

enemy_image = pygame.image.load("images/talita.png")
enemy_image = pygame.transform.scale(enemy_image, (60, 60))

background_image = pygame.image.load("images/fondo.jpg")
background_image = pygame.transform.scale(background_image, (width, height))

# Jugador
player_rect = player_image.get_rect()
player_rect.topleft = (width // 2 - player_rect.width // 2,
                       height - player_rect.height - 10)
player_speed = 15

# Bala
bullet_rect = bullet_image.get_rect()
bullet_speed = 10
bullets = []

# Enemigo
enemy_rect = enemy_image.get_rect()
enemy_speed = 5
enemies = []

# Reloj
clock = pygame.time.Clock()

# Mantener registro de teclas presionadas
keys_pressed = {'left': False, 'right': False}


def print_score_and_high_score():
    global score_text, high_score_text
    score_font = pygame.font.Font(None, 27)
    score_text = score_font.render(f"Score: {score}", True, (255, 255, 255))
    high_score_text = score_font.render(f"High Score: {high_score}", True, (255, 255, 255))


def draw_countdown():
    # Calcular el tiempo restante
    seconds = (10000 - (pygame.time.get_ticks() - start_ticks)) // 1000
    if seconds < 0:
        seconds = 0
    # Crear el texto del temporizador
    font = pygame.font.Font(None, 50)
    text = font.render(f"Reiniciando en: {seconds}", True, (255, 255, 255))
    # Dibujar el texto en la pantalla
    screen.blit(text, (width // 2 - text.get_width() // 2, height // 2 - text.get_height() // 2 + 50))


def reset_game():
    global score, pause, bullets, enemies, player_rect, keys_pressed
    score = 0
    pause = False
    bullets = []
    enemies = []
    player_rect.topleft = (width // 2 - player_rect.width // 2, height - player_rect.height - 10)
    keys_pressed = {'left': False, 'right': False}


def show_game_over_screen():
    global pause, start_ticks
    pause = True
    start_ticks = pygame.time.get_ticks()
    lose_font = pygame.font.Font(None, 120)
    game_over_text = lose_font.render("Perdiste", True, (255, 255, 255))
    print_score_and_high_score()

    while pygame.time.get_ticks() - start_ticks < 10000:
        for events in pygame.event.get():
            if events.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        screen.blit(background_image, (0, 0))
        draw_countdown()
        screen.blit(game_over_text,
                    (width // 2 - game_over_text.get_width() // 2, height // 2 - game_over_text.get_height() // 2))
        screen.blit(score_text, (width // 2 - score_text.get_width() // 2, height // 2 - score_text.get_height() // 2 + 50))
        screen.blit(high_score_text,
                    (width // 2 - high_score_text.get_width() // 2, height // 2 - high_score_text.get_height() // 2 + 80))
        pygame.display.flip()
        pygame.time.wait(1000)

    reset_game()


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        # Manejar movimientos del jugador
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                pause = not pause
            elif not pause:
                if event.key == pygame.K_LEFT:
                    keys_pressed['left'] = True
                elif event.key == pygame.K_RIGHT:
                    keys_pressed['right'] = True
                elif event.key == pygame.K_SPACE:
                    bullet_rect = bullet_image.get_rect()
                    bullet = {
                        'rect': pygame.Rect(
                            player_rect.x +
                            player_rect.width // 2 -
                            bullet_rect.width // 2,
                            player_rect.y,
                            bullet_rect.width,
                            bullet_rect.height
                        ),
                        'image': bullet_image
                    }
                    bullets.append(bullet)

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                keys_pressed['left'] = False
            elif event.key == pygame.K_RIGHT:
                keys_pressed['right'] = False

    # Actualizar posición del jugador
    if keys_pressed['left'] and player_rect.left > 0:
        player_rect.x -= player_speed
    if keys_pressed['right'] and player_rect.right < width:
        player_rect.x += player_speed

    if not pause:
        # Actualizar posición de las balas
        for bullet in bullets:
            bullet['rect'].y -= bullet_speed

        # Generar enemigos aleatorios
        if random.randint(0, 100) < 5:
            enemy_rect = enemy_image.get_rect()
            enemy_rect.x = random.randint(0,
                                          width - enemy_rect.width)
            enemies.append(enemy_rect.copy())

        # Actualizar posición de los enemigos
        for enemy in enemies:
            enemy.y += enemy_speed

    # Colisiones entre balas y enemigos
    for bullet in bullets:
        for enemy in enemies:
            if enemy.colliderect(bullet['rect']):
                bullets.remove(bullet)
                enemies.remove(enemy)
                score += 1  # Incrementar el marcador

    # Colisiones entre jugador y enemigos
    for enemy in enemies:
        if player_rect.colliderect(enemy):
            # Guardar el puntaje más alto
            if score > high_score:
                high_score = score
                with open("score.txt", "w") as file:
                    file.write(str(high_score))
            show_game_over_screen()

    # Limpiar la pantalla con el fondo
    screen.blit(background_image, (0, 0))

    # Dibujar al jugador
    screen.blit(player_image, player_rect)

    # Dibujar las balas
    for bullet in bullets:
        screen.blit(bullet['image'], bullet['rect'].topleft)

    # Dibujar los enemigos
    for enemy in enemies:
        screen.blit(enemy_image, enemy)

    # Renderizar el marcador
    print_score_and_high_score()
    screen.blit(score_text, (10, 10))
    screen.blit(high_score_text, (10, 40))

    pause_font = pygame.font.Font(None, 120)
    # Mostrar mensaje de pausa
    if pause:
        pause_text = pause_font.render("PAUSED", True, (255, 255, 255))
        screen.blit(pause_text, (width // 2 - pause_text.get_width() // 2, height // 2 - pause_text.get_height() // 2))

    # Actualizar la pantalla
    pygame.display.flip()

    # Establecer límite de FPS
    clock.tick(30)
